import { Component, OnInit } from '@angular/core';
import { Person, rgp } from '../stuff';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  name: string = 'Global Discharge'

  person: Person = rgp;
}

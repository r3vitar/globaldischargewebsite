import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'redirect',
  template: `
  <div style="min-height: calc(100vh - 128px)">
    <div i18n>
      redirecting...
    </div>
  </div>`
})
export class RedirectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent, NavList, NavTitle } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LandingComponent } from './landing/landing.component';
import { TeamComponent } from './team/team.component';
import { ContactComponent } from './contact/contact.component';
import { ImprintComponent } from './imprint/imprint.component';
import { SupportComponent } from './support/support.component';
import { FourOFourComponent } from './four-o-four/four-o-four.component';
import { MerchComponent } from './merch/merch.component';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RedirectComponent } from './redirect/redirect.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ArticleComponent } from './article/article.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LandingComponent,
    TeamComponent,
    ContactComponent,
    ImprintComponent,
    NavTitle,
    NavList,
    SupportComponent,
    FourOFourComponent,
    MerchComponent,
    RedirectComponent,
    PrivacyComponent,
    ArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule
  ],
  providers: [
    {
        provide: 'externalUrlRedirectResolver',
        useValue: (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) =>
        {
            window.location.replace((route.data as any).externalUrl);
        }
    }
],
bootstrap: [AppComponent]
})
export class AppModule { }

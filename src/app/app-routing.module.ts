import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { FourOFourComponent } from './four-o-four/four-o-four.component';
import { ImprintComponent } from './imprint/imprint.component';
import { LandingComponent } from './landing/landing.component';
import { MerchComponent } from './merch/merch.component';
import { RedirectComponent } from './redirect/redirect.component';
import { SupportComponent } from './support/support.component';
import { TeamComponent } from './team/team.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home',
  }, {
    path: 'home',
    component: LandingComponent,
  }, {
    path: 'team',
    component: TeamComponent,
  }, {
    path: 'imprint',
    component: ImprintComponent
  }, {
    path: 'impressum',
    component: ImprintComponent
  }, {
    path: 'contact',
    component: ContactComponent
  }, {
    path: 'support',
    component: RedirectComponent,
    resolve: {
      url: 'externalUrlRedirectResolver'
    },
    data: {
      externalUrl: 'https://paypal.me/pools/c/8ydMfwQEHy'
    }
  }, {
    path: 'merch',
    component: RedirectComponent,
    resolve: {
      url: 'externalUrlRedirectResolver'
    },
    data: {
        externalUrl: 'https://shop.spreadshirt.at/globaldischarged/'
    }
  }, {
    path: '**',
    component: FourOFourComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

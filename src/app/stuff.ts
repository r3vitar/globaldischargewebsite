export interface Person {
    company?: string;
    address: Address;
    name: string;
    mail: string;
    phone: string;
}

export interface Address {
    street: string;
    number: number | string,
    door?: number | string,
    plz: number,
    city: string,
    country: string
}

export const rgp: Person = {
    name: 'Rainer Plöchl',
    mail: 'rainer.ploechl@globaldischarge.at',
    phone: '+43 664 1004922',
    address: {
        city: 'Freistadt',
        country: 'AUSTRIA',
        number: 1,
        plz: 4240,
        street: 'Johannisfeldstraße'
    }
}

export const DARK_MODE: string = 'dark-mode'
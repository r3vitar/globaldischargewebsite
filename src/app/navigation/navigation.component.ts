import { ChangeDetectionStrategy, Component, ContentChild, ContentChildren, Directive, ElementRef, EventEmitter, Output, ViewEncapsulation} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { RouterOutlet } from '@angular/router';

/** Title of a card, needed as it's used as a selector in the API. */
@Directive({
  selector: `nav-title, [nav-title], [navTitle]`,
  exportAs: 'navTitle',
  host: {
    class: 'nav-title',
  },
})
export class NavTitle {}

@Directive({
  selector: `nav-list, [nav-list], [navList]`,
  exportAs: 'navList',
  host: {
    class: 'nav-list',
  },
})
export class NavList {}


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  @ContentChild(NavTitle) _title: NavTitle | undefined;


  @Output() darkModeToggle = new EventEmitter<void>();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  toggleDarkMode() {
    this.darkModeToggle.emit();
  }

  battery() {
    const nav = window.navigator as any;
    nav.getBattery().then(function(batt: any) {
      console.log(batt);
      // ... and any subsequent updates.
      batt.onlevelchange = function() {
        console.log(this);
      };
      alert(`Your device is ${batt.charging ? '' : 'not '} charging now. You have ${batt.charging ? batt.dischargingTime + ' battery left' : batt.chargingTime + ' remaining to get fully charged' }`)
    });
  }

  constructor(private breakpointObserver: BreakpointObserver) {}

}

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { DARK_MODE } from './stuff';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isDarkMode: boolean = false;

  title = 'globaldischarge';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );


  constructor(private breakpointObserver: BreakpointObserver,
    private router: Router) {
    const d = localStorage.getItem(DARK_MODE);
    if (d === null || d === undefined) {
      this.isDarkMode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? true : false
    } else {
      this.isDarkMode = JSON.parse(d);
    }
    localStorage.setItem(DARK_MODE, JSON.stringify(this.isDarkMode));
  }

  darkModeToggle() {
    this.isDarkMode = !this.isDarkMode;
    localStorage.setItem(DARK_MODE, JSON.stringify(this.isDarkMode));
    location.reload();
  }
}
